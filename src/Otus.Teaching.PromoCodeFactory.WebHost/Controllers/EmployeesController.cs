﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.UseCases;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly EmployeeInteractor _employeeInteractor;


        public EmployeesController(EmployeeInteractor employeeInteractor)
        {
            _employeeInteractor = employeeInteractor;
        }
        

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeInteractor.GetAll().ConfigureAwait(false);

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeInteractor.Get(id).ConfigureAwait(false);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        [HttpPost("Create")]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployee(EmployeeForm form)
        {
            var employee = await _employeeInteractor.CreateOrUpdateEmployee(form).ConfigureAwait(false); 
            if (employee == null)
            {
                return BadRequest();
            }

            return await this.GetEmployeeByIdAsync(employee.Id).ConfigureAwait(false);
        }

        [HttpPut("Update/{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployee(Guid? id, EmployeeForm form)
        {
            var employee = await _employeeInteractor.CreateOrUpdateEmployee(form, id).ConfigureAwait(false);
            if (employee == null)
            {
                return BadRequest();
            }

            return await this.GetEmployeeByIdAsync(employee.Id).ConfigureAwait(false);
        }

        [HttpDelete("Delete/{id:guid}")]
        public async Task<ActionResult> RemoveEmployee(Guid id)
        {
            if (await _employeeInteractor.Remove(id).ConfigureAwait(false))
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }
    }
}