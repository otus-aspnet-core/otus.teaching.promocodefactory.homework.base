﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeForm : IEmployeeForm
    {         
        public string FirstName { get; set; }
         
        public string LastName { get; set; }
         
        public string Email { get; set; }
         
        public int AppliedPromocodesCount { get; set; }
         
        public IReadOnlyCollection<Guid> RoleIds { get; set; }
    }
}
