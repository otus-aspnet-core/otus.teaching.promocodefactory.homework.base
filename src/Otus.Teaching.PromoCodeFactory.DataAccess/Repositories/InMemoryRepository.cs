﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected ICollection<T> Data { get; set; }

        public InMemoryRepository(ICollection<T> data)
        {
            Data = data ?? new List<T>();
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> AddAsync(T entity)
        {
            if (entity != null)
            {
                Data.Add(entity);

                return Task.FromResult(entity);
            }

            return null;
        }

        public async Task<T> UpdateAsync(T entity)
        {
            if (entity != null)
            {
                var entityData = await GetByIdAsync(entity.Id).ConfigureAwait(false);
                if (TryRemove(entityData))
                {
                    return await AddAsync(entity).ConfigureAwait(false);
                }
            }

            return null;
        }

        public async Task<bool> RemoveAsync(Guid id)
        {
            var entity = await GetByIdAsync(id).ConfigureAwait(false);

            return TryRemove(entity);
        }

        private bool TryRemove(T entity)
        {
            if (entity != null)
            {
                Data.Remove(entity);

                return true;
            }

            return false;
        }
    }
}