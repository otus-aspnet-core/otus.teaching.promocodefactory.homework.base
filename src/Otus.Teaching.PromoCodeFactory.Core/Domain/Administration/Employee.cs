﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public List<Role> Roles { get; set; }

        public int AppliedPromocodesCount { get; set; }


        public Employee()
        { }

        public Employee(Guid? id, IEmployeeForm employeeForm)
        {
            this.Update(employeeForm);

            this.Id = id ?? Guid.NewGuid();
        }


        public Employee SetRoles(List<Role> roles)
        {
            this.Roles = roles ?? this.Roles ?? new List<Role>();

            return this;
        }

        public Employee Update(IEmployeeForm employeeForm)
        {
            if (employeeForm == null)
            {
                throw new ArgumentNullException(nameof(employeeForm));
            }

            this.FirstName = employeeForm.FirstName ?? this.FirstName;
            this.LastName = employeeForm.LastName ?? this.LastName;

            this.Email = employeeForm.Email ?? this.Email;

            this.AppliedPromocodesCount = employeeForm.AppliedPromocodesCount >= 0
                ? employeeForm.AppliedPromocodesCount
                : this.AppliedPromocodesCount;

            return this;
        }
    }
}