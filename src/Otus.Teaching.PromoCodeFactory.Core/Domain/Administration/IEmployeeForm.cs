﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public interface IEmployeeForm
    {
        string FirstName { get; set; }

        string LastName { get; set; }

        string Email { get; set; }

        int AppliedPromocodesCount { get; set; }

        IReadOnlyCollection<Guid> RoleIds { get; set; }
    }
}
