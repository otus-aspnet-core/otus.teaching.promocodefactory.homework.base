﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.UseCases
{
    public class EmployeeInteractor
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeeInteractor (IRepository<Employee> employeeRepository,
            IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }

        public async Task<Employee> CreateOrUpdateEmployee(IEmployeeForm employeeForm, Guid? id = default)
        {
            if (employeeForm != null)
            {
                List<Role> roles = default;

                if (employeeForm.RoleIds?.Any() == true)
                {
                    roles = (await _roleRepository.GetAllAsync().ConfigureAwait(false))?.ToList();
                    if (roles?.Any() == true)
                    {
                        roles = roles.Where(x => employeeForm.RoleIds.Contains(x.Id)).ToList();
                    }
                }

                Employee employee = default;

                if (id.HasValue)
                {
                    employee = await this.Get(id.Value).ConfigureAwait(false);
                }

                return employee == default
                    ? await _employeeRepository.AddAsync(new Employee(id, employeeForm).SetRoles(roles)).ConfigureAwait(false)
                    : await _employeeRepository.UpdateAsync(employee.Update(employeeForm).SetRoles(roles)).ConfigureAwait(false);
            }

            return null;
        }

        public async Task<Employee> Get(Guid id)
        {
            return await _employeeRepository.GetByIdAsync(id).ConfigureAwait(false);
        }

        public async Task<IEnumerable<Employee>> GetAll()
        {
            return await _employeeRepository.GetAllAsync().ConfigureAwait(false);
        }

        public async Task<bool> Remove(Guid id)
        {
            return await _employeeRepository.RemoveAsync(id).ConfigureAwait(false);
        }
    }
}
